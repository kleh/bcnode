const SHA256 = require('crypto-js/sha256');

class Block {
  constructor(index, data, timestamp, previoushash = ''){
    this.data = data;
    this.index = index;
    this.previoushash = previoushash;
    this.timestamp = timestamp;
    this.hash = this.calculateHash();
  }

  calculateHash(){
    return SHA256(this.index + this.previoushash + this.timestamp + JSON.stringify(this.data)).toString();
  }
}

module.exports = Block;
