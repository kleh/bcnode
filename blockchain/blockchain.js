var Block = require('./blocknode.js');

class Blockchain {
  constructor(){
    this.chain = [ this.createRootBlock() ];
  }

  createRootBlock(){
    return new Block(0, "ROOT", (new Date()).toString(), '0' );
  }

  getLatestBlock(){
    return this.chain[this.chain.length -1];
  }

  addBlock(newBlock){
    newBlock.previoushash = this.getLatestBlock().hash;
    newBlock.hash = newBlock.calculateHash();
    this.chain.push(newBlock);
  }
}

module.exports = Blockchain;
